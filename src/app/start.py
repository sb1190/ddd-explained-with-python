from src.app.server.server import Server


__server = Server()
__server.register_dependencies()
flask_app = __server.get_http_server()
