import os

from flask import Flask


from src.app.server.init_extensions import InitExtensions
from src.app.server.register_blueprint import RegisterBlueprint


class Server:
    __server: Flask = None

    def __init__(self):
        self.__server = Flask(__name__)
        self.__server.config.from_object(
            os.environ.get(
                "APP_SETTINGS",
                "src.app.server.environments.development_env.DevelopmentEnv",
            )
        )

    def register_dependencies(self):
        InitExtensions(self.__server)
        RegisterBlueprint(self.__server)

    def get_http_server(self) -> Flask:
        return self.__server
