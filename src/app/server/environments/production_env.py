from src.app.server.environments.base_env import BaseEnv


class ProductionEnv(BaseEnv):
    raise NotImplementedError
