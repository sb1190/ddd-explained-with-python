from src.app.server.environments.base_env import BaseEnv


class TestingEnv(BaseEnv):
    raise NotImplementedError
