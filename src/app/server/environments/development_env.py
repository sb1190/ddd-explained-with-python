from src.app.server.environments.base_env import BaseEnv


class DevelopmentEnv(BaseEnv):
    ENVIRONMENT = "development"
    DEBUG = True

    # Host
    HOST = "http://localhost:5000"

    SECRET_KEY = "super_secret_key"
    SECURITY_PASSWORD_SALT = "my_precious_two"

    SQLALCHEMY_DATABASE_URI = "postgresql://postgres:qwer@localhost:5432/users"
    PROPAGATE_EXCEPTIONS = True
