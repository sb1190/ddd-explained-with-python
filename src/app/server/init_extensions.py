from flask import Flask

from src.app.server.extensions import *
from src.app.before_start_server import db_to_run_unit_tests


class InitExtensions:

    def __init__(self, server: Flask):
        jwt.init_app(server)
        mail.init_app(server)
        if db_to_run_unit_tests is not None:
            server.config["SQLALCHEMY_DATABASE_URI"] = db_to_run_unit_tests
            server.flask_app.config["DEBUG"] = False
            server.flask_app.config["PROPAGATE_EXCEPTIONS"] = True
        db.init_app(server)
        migrate.init_app(server, db)
        cors.init_app(server, resources={r"/api/v1/*": {"origins": "*"}})
