from flask_migrate import Migrate, MigrateCommand
from flask_script import Manager

from src.app.server.extensions import db
from src.app.server.auth_app import AuthApp

migrate = Migrate(AuthApp(), db)
manager = Manager(AuthApp())

manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
