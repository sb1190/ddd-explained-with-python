## Authentication and user information management projects to explain ddd

---

### Description

This repository seeks to explain ddd architecture in a modularized monolith. Modularized monoliths seek to streamline software development without sacrificing scalability as the business moves forward. 

---

### Technology stack

🔗Programming language:  [Python](https://www.python.org/)
🔗Web framework: [Flask](https://flask.palletsprojects.com/en/3.0.x/)
🔗Database: [Postgresql](https://www.postgresql.org/)
🔗ORM: [Flask-Sqlalchemy](https://flask-sqlalchemy.palletsprojects.com/en/3.1.x/)
🔗unit tests:  [UnitTest](https://docs.python.org/3/library/unittest.html) 
🔗Envs:  [Python-dotenv](https://pypi.org/project/python-dotenv/)

---

### Diagram

![](./documents/base-architecture-proposal-modularized-monolith.png){width='1000px'}

---

### Running the project 

1. Configure a virtual environment with the dependencies exposed in the requirements.txt for this you can use [miniconda](https://docs.anaconda.com/free/miniconda/index.html), [pyenv](https://github.com/pyenv/pyenv) or [poetry](https://python-poetry.org/) the one of your choice is perfect.

2. Run the flask run command and it will take the configuration found in the .env file.

3. Run the command `pytest --cov=. --cov-report=html tests/`command to verify the coverage of the tests, it will generate the htmlcov folder in the root of the project, find the index.html file inside it and open it in the browser.


---

_by [Sebastián Bermúdez Restrepo](https://www.linkedin.com/in/sb1190/)_
